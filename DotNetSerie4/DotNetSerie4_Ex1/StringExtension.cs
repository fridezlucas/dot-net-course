﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie4_Ex1
{
    public static class StringExtension
    {
        /// <summary>
        /// Reverse the string given in param
        /// </summary>
        /// <param name="s">string ot reverse</param>
        /// <returns>reversed string</returns>
        public static string ToStringReverse(this string s)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = s.Length - 1; i > -1; --i)
            {
                sb.Append(s[i]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Change lower case to upper case and vice-versa in a string given in param
        /// </summary>
        /// <param name="s">string to adapt</param>
        /// <returns>adapted string</returns>
        public static string UpLowUp(this string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in s)
            {
                if (Char.IsLower(c))
                {
                    sb.Append(Char.ToUpper(c));
                }
                else if (Char.IsUpper(c))
                {
                    sb.Append(Char.ToLower(c));
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}
