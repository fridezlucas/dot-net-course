﻿using DotNetSerie2_TicTacToe.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.TicTacToe
{
    /// <summary>
    /// Representation of TicTacToe Board
    /// </summary>
    public class Board : IBoard
    {
        /// <summary>
        /// Board Size (rows and cols)
        /// </summary>
        public static readonly int NUMBER_CASES = 3;

        /// <summary>
        /// Board representation
        /// </summary>
        public Symbol[][] Cases { get; set; }

        /// <summary>
        /// Instanciate a new Board
        /// </summary>
        public Board()
        {
            Cases = new Symbol[NUMBER_CASES][];
            for (int r = 0; r < NUMBER_CASES; ++r)
            {
                Cases[r] = new Symbol[NUMBER_CASES];
            }
        }

        /// <summary>
        /// Define if a cell is empty
        /// </summary>
        /// <param name="number">number of cell (according to Printer method)</param>
        /// <returns>true if cell is empty, false otherwise</returns>
        public bool IsCellEmpty(int number)
        {
            (int, int) cell = Board.GetCellAccordingNumber(number);

            return Cases[cell.Item1][cell.Item2] == Symbol.Empty;
        }

        /// <summary>
        /// Get a row and col for a cell number
        /// </summary>
        /// <param name="number">cell number</param>
        /// <returns>(row, col)</returns>
        public static (int, int) GetCellAccordingNumber(int number)
        {
            int row = number / NUMBER_CASES;
            int col = number % NUMBER_CASES;

            return (row, col);
        }

        /// <summary>
        /// Indexers
        /// </summary>
        /// <param name="row">row number</param>
        /// <returns>Symbol row</returns>
        public Symbol[] this[int row] => Cases[row];

        /// <summary>
        /// Indexers
        /// </summary>
        /// <param name="row">row number</param>
        /// <param name="col">col number</param>
        /// <returns>Symbol case</returns>
        public Symbol this[int row, int col] { get => Cases[row][col]; set => Cases[row][col] = value; }

        /// <summary>
        /// Get Number of Cases
        /// </summary>
        public int Length => Cases.Length;

        /// <summary>
        /// Define if Board has an empty cell
        /// </summary>
        /// <returns>true if an empty cell exists, false otherwise</returns>
        public bool IsFull()
        {
            foreach(Symbol[] row in Cases)
            {
                foreach(Symbol cell in row)
                {
                    if (cell == Symbol.Empty)
                        return false;
                }
            }
            return true;
        }
    }
}
