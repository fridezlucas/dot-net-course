﻿using DotNetSerie2_TicTacToe.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DotNetSerie2_TicTacToe
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Get a T attribute
        /// </summary>
        /// <typeparam name="TAttribute">attribute type</typeparam>
        /// <param name="value">enum value</param>
        /// <returns>custom attribute</returns>
        public static TAttribute GetAttribute<TAttribute>(this System.Enum value)
            where TAttribute : System.Attribute
        {
            var type = value.GetType();
            var name = System.Enum.GetName(type, value);
            return type.GetField(name)
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();
        }
    }
}
