﻿using System;
using System.Runtime.InteropServices.ComTypes;

namespace DotNetSerie1_Ex1
{
    class Program
    {
        /// <summary>
        /// Print on console information about each Point of given array
        /// </summary>
        /// <param name="points">array of Points3D to print on console</param>
        public static void ShowPointsInfo(Point3D[] points)
        {
            foreach (Point3D p in points)
            {
                Console.WriteLine(p);
            }
        }

        /// <summary>
        /// Do the exercise 1
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Point3D p1 = new Point3D(2, 3, 4);
            Point3D p2 = new Point3D(6, 7, 8);

            Point3D[] points = { p1, p2 };

            Console.WriteLine("Before inverting points...");
            ShowPointsInfo(points);
            Point3D.SwapPoints(ref points[0], ref points[1]);
            Console.WriteLine("After inverting points...");
            ShowPointsInfo(points);
        }
    }
}
