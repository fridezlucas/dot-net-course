﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.TicTacToe
{
    /// <summary>
    /// Represent an EndGameStep
    /// </summary>
    public struct EndGameStep
    {
        /// <summary>
        /// Define if there is a Winner
        /// </summary>
        public bool Winner { get; set; }

        /// <summary>
        /// Define if the Game is finished
        /// </summary>
        public bool EndGame { get; set; }

        /// <summary>
        /// EndGameStep instanciation
        /// </summary>
        /// <param name="winner">winner</param>
        /// <param name="endGame">endGame</param>
        public EndGameStep(bool winner, bool endGame)
        {
            Winner = winner;
            EndGame = endGame;
        }
    }
}
