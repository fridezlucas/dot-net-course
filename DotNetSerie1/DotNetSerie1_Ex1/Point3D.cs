﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie1_Ex1
{
    public struct Point3D
    {
     
        /// <summary>
        /// Instanciate a new Point3D
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Point3D(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        /// <summary>
        /// Override ToString() method
        /// </summary>
        /// <returns>String representation of current Point3D</returns>
        public override string ToString()
        {
            return $"Point 3D {{x = {X}, y = {Y}, z = {Z} }}, {DistanceToOrigin()} away from origin";
        }

        /// <summary>
        /// Calculate distance to Origin (0,0,0)
        /// </summary>
        /// <returns>distance to origin</returns>
        public double DistanceToOrigin()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2));
        }

        /// <summary>
        /// Swap 2 Points values
        /// </summary>
        /// <param name="p1">Point3D 1</param>
        /// <param name="p2">Point3D 3</param>
        public static void SwapPoints(ref Point3D p1, ref Point3D p2)
        {
            Point3D tmp = p2;
            p2 = p1;
            p1 = tmp;
        }
    }
}
