﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace DotNetSerie1_Ex3
{
    public class MeasureFile
    {
        private const int MEASURE_CHAR_WIDTH = 3;

        /// <summary>
        /// Instanciate a new MeasureFile
        /// </summary>
        /// <param name="filename">Path to the file to measure</param>
        public MeasureFile(string filename)
        {
            Filename = filename;
        }

        public string Filename { get; set; }

        /// <summary>
        /// Read all file and print according measure
        /// </summary>
        public void ReadAllFile()
        {
            List<List<int>> measures = new List<List<int>>();
            List<int> measureLine = new List<int>();

            using FileStream fs = File.OpenRead(Filename);
            using StreamReader sr = new StreamReader(fs);
            String line;

            // Read file line by line
            while ((line = sr.ReadLine()) != null)
            {
                // Convert and store measure
                int m;
                try
                {
                    m = Convert.ToInt32(line);
                }
                catch (Exception e)
                {
                    m = -1;
                }
                if (measureLine.Count == 10)
                {
                    measures.Add(measureLine);
                    measureLine = new List<int>();
                }
                measureLine.Add(m);
            }
            measures.Add(measureLine);

            PrintMeasure(measures);
        }

        /// <summary>
        /// Print the measures on console (multiple lines)
        /// </summary>
        /// <param name="measures">List of all measure lists</param>
        private static void PrintMeasure(List<List<int>> measures)
        {
            foreach (List<int> line in measures)
            {
                PrintMeasureLine(line);
            }
        }

        /// <summary>
        /// Print a measure list on console (1 line)
        /// </summary>
        /// <param name="measureLine">measure list</param>
        private static void PrintMeasureLine(List<int> measureLine)
        {
            StringBuilder sb = new StringBuilder();
            int listCount = measureLine.Count;
            for (int i = 0; i < listCount; ++i)
            {
                sb.Append($"{measureLine[i],MEASURE_CHAR_WIDTH}{(i == listCount - 1 ? "" : ",")} ");
            }

            Console.WriteLine(sb.ToString());
        }

    }
}
