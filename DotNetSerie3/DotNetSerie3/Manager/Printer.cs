﻿using DotNetSerie3.Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie3.Manager
{
    /// <summary>
    /// Printer to print in console
    /// </summary>
    public static class Printer
    {
        /// <summary>
        /// Print a list of cantons
        /// </summary>
        /// <param name="lstCantons">list of cantons</param>
        public static void PrintAllCantons(List<string> lstCantons)
        {
            Console.WriteLine("All cantons : ");
            lstCantons.ForEach(c => Console.WriteLine(c));
            Console.WriteLine("----------------------");
        }

        /// <summary>
        /// Print a list of stations
        /// </summary>
        /// <param name="lstStations">list of stations</param>
        public static void PrintAllStations(List<Station> lstStations)
        {
            Console.WriteLine("All stations : ");
            lstStations.ForEach(s => Console.WriteLine(s));
            Console.WriteLine("----------------------");
        }

        /// <summary>
        /// Get string value
        /// </summary>
        /// <param name="str">string to check</param>
        /// <returns>string value or ? if null</returns>
        public static string GetValue(string str)
        {
            if (str == null)
            {
                return "?";
            }
            else
            {
                return str;
            }
        }

        /// <summary>
        /// Get Position string
        /// </summary>
        /// <param name="pos">position to print</param>
        /// <returns>string value or ? if null</returns>
        public static string GetValue(GeographicPosition pos)
        {
            if (pos == null)
            {
                return "?";
            }
            else
            {
                return pos.ToString();
            }
        }

        /// <summary>
        /// Get integer string value
        /// </summary>
        /// <param name="integer">integer to print</param>
        /// <returns>string value or ? if null</returns>
        public static string GetValue(int? integer)
        {
            if (!integer.HasValue)
            {
                return "?";
            }
            else
            {
                return integer.ToString();
            }
        }
    }
}
