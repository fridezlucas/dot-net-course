﻿using DotNetSerie2_TicTacToe.Extensions;
using DotNetSerie2_TicTacToe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.TicTacToe
{
    /// <summary>
    /// Represent a TicTacToe Game
    /// </summary>
    public class Game : ITerminable
    {
        /// <summary>
        /// Player 1
        /// </summary>
        Player Player1 { get; set; }

        /// <summary>
        /// Player 2
        /// </summary>
        Player Player2 { get; set; }

        /// <summary>
        /// TicTacToe Board
        /// </summary>
        Board GameBoard { get; set; }

        /// <summary>
        /// Instanciate a new TicTacToe Game
        /// </summary>
        /// <param name="player1">First Player</param>
        /// <param name="player2">Second Player</param>
        public Game(Player player1, Player player2)
        {
            Player1 = player1;
            Player2 = player2;
            GameBoard = new Board();
        }

        /// <summary>
        /// Get next player according to current one
        /// </summary>
        /// <param name="currentPlayer">current player who played</param>
        private void GetNextPlayer(ref Player currentPlayer)
        {
            currentPlayer = currentPlayer == Player1 ? Player2 : Player1;
        }

        /// <summary>
        /// Run TicTacToe game
        /// </summary>
        public void Run()
        {
            // Define first player according to symbol (official rule)
            Player currentPlayer = Player1.Option.GetAttribute<PlayFirstAttribute>().PlayFirst ? Player1 : Player2;

            // Run the game
            EndGameStep step;
            do
            {
                currentPlayer.Play(GameBoard);
                GetNextPlayer(ref currentPlayer);
                step = CanPlay();
            } while (!step.EndGame);

            ManageEndGame(step.Winner, currentPlayer);
        }

        /// <summary>
        /// Show Winner (if there is one) and ask to replay
        /// </summary>
        /// <param name="winner">boolean variable that define if there is a winner</param>
        /// <param name="lastPlayer">last player that played</param>
        private void ManageEndGame(bool winner, Player lastPlayer)
        {
            // Game finished !
            Printer.PrintBoard(GameBoard);
            if (winner)
            {
                GetNextPlayer(ref lastPlayer);
                lastPlayer.Score++;
                Printer.PrintWinner(lastPlayer);
            }
            if (Printer.AskReplay())
            {
                GameBoard = new Board();
                Run();
            }
        }

        /// <summary>
        /// Check if there is a stroke of 3 in lines
        /// </summary>
        /// <returns>true if there is a winner, false otherwise</returns>
        private bool CheckLines()
        {
            for (int r = 0; r < Board.NUMBER_CASES; ++r)
            {
                if (GameBoard[r][0] == GameBoard[r][1] && GameBoard[r][0] == GameBoard[r][2] && GameBoard[r][0] != Symbol.Empty)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Check if there is a stroke of 3 in columns
        /// </summary>
        /// <returns>true if there is a winner, false otherwise</returns>
        private bool CheckColumns()
        {
            for (int r = 0; r < Board.NUMBER_CASES; ++r)
            {
                if (GameBoard[0][r] == GameBoard[1][r] && GameBoard[0][r] == GameBoard[2][r] && GameBoard[0][r] != Symbol.Empty)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Check if there is a stroke of 3 in ties
        /// </summary>
        /// <returns>true if there is a winner, false otherwise</returns>
        private bool CheckTies()
        {
            return (GameBoard[0][0] == GameBoard[1][1] && GameBoard[0][0] == GameBoard[2][2] && GameBoard[0][0] != Symbol.Empty) ||
                (GameBoard[0][2] == GameBoard[1][1] && GameBoard[0][2] == GameBoard[2][0] && GameBoard[0][2] != Symbol.Empty);
        }

        /// <summary>
        /// Check if there is a stroke of 3 on Board
        /// </summary>
        /// <returns>true if there is a winner, false otherwise</returns>
        public bool HasStrokeOf3()
        {
            return CheckColumns() || CheckLines() || CheckTies();
        }

        /// <summary>
        /// Define if next player can play
        /// </summary>
        /// <returns>true if player can play, false otherwise</returns>
        public EndGameStep CanPlay()
        {
            bool stroke = HasStrokeOf3();
            return new EndGameStep(stroke, stroke || GameBoard.IsFull());
        }
    }
}
