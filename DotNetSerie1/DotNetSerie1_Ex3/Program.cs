﻿using System;

namespace DotNetSerie1_Ex3
{
    class Program
    {
        /// <summary>
        /// Analyse a file and print its measures
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            MeasureFile f = new MeasureFile(@"C:\DEV\HES\dotnet\dot-net-course\DotNetSerie1\DotNetSerie1_Ex3\Ressources\Mesures.txt");
            f.ReadAllFile();
        }
    }
}
