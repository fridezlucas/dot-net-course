﻿using DotNetSerie3.Manager;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading;

namespace DotNetSerie3.Logic
{
    /// <summary>
    /// Represent a ski station
    /// </summary>
    public class Station
    {
        /// <summary>
        /// Instanciate a new station
        /// </summary>
        public Station()
        {
            this.Name = "";
        }

        /// <summary>
        /// Name of Station
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Domain of Station
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Canton of station (2 letters)
        /// </summary>
        public string Canton { get; set; }

        /// <summary>
        /// Altitude of station
        /// </summary>
        public int? Altitude { get; set; }

        /// <summary>
        /// Max altitude of station
        /// </summary>
        public int? AltitudeMax { get; set; }

        /// <summary>
        /// Position of Station (long, lat)
        /// </summary>
        public GeographicPosition Position { get; set; }

        /// <summary>
        /// Adult price of station
        /// </summary>
        public int TarifAdult { get; set; }

        /// <summary>
        /// Child price ot station
        /// </summary>
        public int TarifChild { get; set; }

        /// <summary>
        /// Lift number of station
        /// </summary>
        public int? LiftNumber { get; set; }

        /// <summary>
        /// Slope number of station
        /// </summary>
        public int? SlopeNumber { get; set; }

        /// <summary>
        /// slope distance of Station [km]
        /// </summary>
        public int? SlopeDistance { get; set; }

        /// <summary>
        /// Syntaxic sugar to compute distance between 2 Station (according their position)
        /// </summary>
        /// <param name="destination">Destination to compute distance</param>
        /// <returns></returns>
        public double DistanceTo(Station destination)
        {
            return Position!.DistanceTo(destination.Position);
        }

        /// <summary>
        /// ToString method to easily print Station in console
        /// </summary>
        /// <returns>string representing Station</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append($"{Printer.GetValue(Name)}, ");
            sb.Append($"{Printer.GetValue(Domain)} ");
            sb.Append($"in {Printer.GetValue(Canton)} ");
            sb.Append($"at {Printer.GetValue(Position)}.\n\t");
            sb.Append($"Alt: [{Printer.GetValue(Altitude)}");
            sb.Append($";{Printer.GetValue(AltitudeMax)}]\n");
            sb.Append($"\tInfos : Tarif Adult {Printer.GetValue(TarifAdult)}");
            sb.Append($", tarif Child {Printer.GetValue(TarifChild)}\n\t");
            sb.Append($"For {Printer.GetValue(LiftNumber)} lifs, ");
            sb.Append($"{Printer.GetValue(SlopeNumber)} slopes ({Printer.GetValue(SlopeNumber)}km)");
            return sb.ToString();
        }
    }
}
