﻿using DotNetSerie3.Builder;
using DotNetSerie3.Logic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace DotNetSerie3.Manager
{
    /// <summary>
    /// Extract data from a csv file
    /// </summary>
    public static class DataExtracter
    {
        /// <summary>
        /// Get all stations from a csv file
        /// </summary>
        /// <returns>list of stations</returns>
        public static List<Station> GetStations()
        {
            List<Station> lstStations = new List<Station>();

            List<string[]> lstStationArray = ReadCSVFile();

            for(int i = 1; i < lstStationArray.Count; ++i)
            {
                string[] line = lstStationArray[i];
                Station currentStation = new StationBuilder()
                    .SetName(line[0])
                    .SetDomain(line[1])
                    .SetCanton(line[2])
                    .SetAltitude(line[3])
                    .SetAltitudeMax(line[4])
                    .SetPosition(line[5], line[6])
                    .SetTarifAdult(line[7])
                    .SetTarifChild(line[8])
                    .SetLiftNumber(line[9])
                    .SetSlopeNumber(line[10])
                    .SetSlopeDistance(line[11])
                    .BuildStation();

                lstStations.Add(currentStation);
            }

            return lstStations;
        }

        /// <summary>
        /// Read a complete csv file that contains stations
        /// </summary>
        /// <returns>list of stations (string array structure)</returns>
        private static List<string[]> ReadCSVFile()
        {
            using var reader = new StreamReader("Resources/SwissSkiDB.csv");
            List<string[]> lstLines = new List<string[]>();
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                var values = line.Split(';');
                lstLines.Add(values);
            }
            return lstLines;
        }
    }
}
