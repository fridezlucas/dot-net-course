﻿using DotNetSerie2_TicTacToe.TicTacToe;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.Interfaces
{
    public interface IBoard
    {
        /// <summary>
        /// Get a row from a Board
        /// </summary>
        /// <param name="row">row number</param>
        /// <returns>row</returns>
        Symbol[] this[int row]
        {
            get;
        }

        /// <summary>
        /// Get a cell from a baord
        /// </summary>
        /// <param name="row">row selected</param>
        /// <param name="col">col selected</param>
        /// <returns>cell</returns>
        Symbol this[int row, int col]
        {
            get; set;
        }

        /// <summary>
        /// get Board length
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Define if Board is full
        /// </summary>
        /// <returns>true if full; otherwise false</returns>
        bool IsFull();
    }
}
