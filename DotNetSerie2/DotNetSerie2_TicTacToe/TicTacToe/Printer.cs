﻿using DotNetSerie2_TicTacToe.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNetSerie2_TicTacToe.TicTacToe
{
    /// <summary>
    /// Static class that help Game to print info on console
    /// </summary>
    public static class Printer
    {
        /// <summary>
        /// Ask symbol to user
        /// </summary>
        /// <returns>Symbol selected</returns>
        public static Symbol AskSymbol()
        {
            Symbol symbol = Symbol.Empty;

            while (symbol == Symbol.Empty)
            {
                Console.Write("Please select a symbol between [x] cross or [o] circle : ");
                string userInput = Console.ReadLine();

                if (userInput == "x")
                    symbol = Symbol.Cross;
                else if (userInput == "o")
                    symbol = Symbol.Circle;
            }

            return symbol;
        }

        /// <summary>
        /// Ask name to user
        /// </summary>
        /// <returns>name entered</returns>
        public static string AskName()
        {
            Console.Write("Please enter your name : ");
            return Console.ReadLine();
        }

        /// <summary>
        /// Ask to user a case in which he wants to play
        /// </summary>
        /// <param name="player">Player</param>
        /// <param name="board">TicTacToe Board</param>
        /// <returns>case number</returns>
        public static int AskUserACase(Player player, Board board)
        {
            int selectedCase;
            do
            {
                PrintBoard(board, true);
                Console.Write($"It's your turn, {player.Name}, Please select a case to play : ");
                try
                {
                    selectedCase = int.Parse(Console.ReadLine());
                } catch(Exception)
                {
                    selectedCase = 0;
                }
            } while (selectedCase < 1 || !board.IsCellEmpty(selectedCase - 1));

            return selectedCase - 1;
        }

        /// <summary>
        /// Print Winner
        /// </summary>
        /// <param name="player">Winner Player</param>
        public static void PrintWinner(Player player)
        {
            Console.WriteLine($"Player {player.Name} ({player.Option.GetAttribute<DescriptionAttribute>().Description}) wins for the {player.Score} time{(player.Score > 1 ? "s" : "")} !");
        }

        /// <summary>
        /// Ask User to replay
        /// </summary>
        /// <returns>true if the wants to replay, false otherwise</returns>
        public static bool AskReplay()
        {
            Console.Write("Would you like to replay ? [y/n] : ");
            return Console.ReadLine() == "y";
        }

        /// <summary>
        /// Print Board
        /// </summary>
        /// <param name="board">Board to print</param>
        /// <param name="fillEmptyCases">if true, shoe cell numbers on empty cells</param>
        public static void PrintBoard(IBoard board, bool fillEmptyCases = false)
        {
            Console.Clear();
            for (int r = 0; r < board.Length; ++r)
            {
                PrintBoardRow(board[r], r, fillEmptyCases);
            }
        }

        /// <summary>
        /// Print a Board row
        /// </summary>
        /// <param name="row">Symbol row</param>
        /// <param name="col">number of col</param>
        /// <param name="fillEmptyCases">if true, must write cells number instead of empty cells</param>
        public static void PrintBoardRow(Symbol[] row, int col, bool fillEmptyCases = false)
        {
            StringBuilder sb = new StringBuilder();

            // Print above line 
            if (col != 0)
            {
                sb.Append("-----+-----+-----\n");
            }

            // Print Content
            for (int i = 0; i < row.Length; ++i)
            {
                string caseChar;

                if (fillEmptyCases && row[i].GetAttribute<DescriptionAttribute>().Description == " ")
                {
                    caseChar = (col * 3 + i + 1).ToString();
                }
                else
                {
                    caseChar = row[i].GetAttribute<DescriptionAttribute>().Description;
                }

                sb.Append($"  {caseChar}  ");

                // Print Border if not last case of row
                if (i != row.Length - 1)
                {
                    sb.Append("|");
                }
            }
            Console.WriteLine(sb);
        }
    }
}
