﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie4_Ex2.Resources
{
    public static class ResourcesHelper
    {
        public static List<Client> GetClientList()
        {
            List<Client> clients = new List<Client>(){
            new Client{ID="A", Ville="New York", Pays="USA", Region="Amerique du Nord", Ventes=8750},
            new Client{ID="B", Ville="Bruxelles", Pays="Belgique", Region="Europe", Ventes=8500},
            new Client{ID="C", Ville="Calcutta", Pays="Inde", Region="Asie", Ventes=345},
            new Client{ID="D", Ville="Dublin", Pays="Irlande", Region="Europe", Ventes=9875},
            new Client{ID="E", Ville="Edimburgh", Pays="Angleterre", Region="Europe", Ventes=4444},
            new Client{ID="F", Ville="Florence", Pays="Italie", Region="Europe", Ventes=6501},
            new Client{ID="G", Ville="Johanesbourg", Pays="Afrique du Sud", Region="Afrique", Ventes=6800},
            new Client{ID="H", Ville="Budapest", Pays="Hongrie", Region="Europe", Ventes=4625},
            new Client{ID="I", Ville="Sao Paulo", Pays="Brésil", Region="Amerique du Sud", Ventes=10340},
            new Client{ID="J", Ville="Québec", Pays="Canada", Region="Amerique du Nord", Ventes=8888},
            new Client{ID="K", Ville="Karachi", Pays="inde", Region="Asie", Ventes=278},
            new Client{ID="L", Ville="Lima", Pays="Perou", Region="Amerique du Sud", Ventes=5890},
            new Client{ID="M", Ville="Marseille", Pays="France", Region="Europe", Ventes=6750}};

            return clients;
        }
    }
}
