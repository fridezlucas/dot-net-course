﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie1_Ex4
{
    class Program
    {
        private static readonly int LIMIT_RANDOM_NUMBER = 100;
        private static readonly int COUNT_RANDOM_NUMBERS = 20;
        private static readonly int INDEX_ODD = 0;
        private static readonly int INDEX_EVEN = 1;

        /// <summary>
        /// Generate a random array with integer numbers between [0;99]
        /// </summary>
        /// <param name="size">size of generated array</param>
        /// <returns>Generated random number array</returns>
        private static int[] GenerateRandomArray(int size)
        {
            int[] randomValues = new int[COUNT_RANDOM_NUMBERS];
            Random rand = new Random();
            
            for(int i = 0; i < COUNT_RANDOM_NUMBERS; ++i)
            {
                randomValues[i] = rand.Next(LIMIT_RANDOM_NUMBER);
            }

            return randomValues;
        }

        /// <summary>
        /// Sort odd and even numbers from a array of integers
        /// </summary>
        /// <param name="numbers">array to sort</param>
        /// <returns>return a sorted tuple of 2 array (odd, even)</returns>
        private static Tuple<int[], int[]> PairImpair(int[] numbers)
        {
            List<int>[] sortedNumbers = { new List<int>(), new List<int>() };
            // Split array : odd and event numbers
            foreach (int e in numbers)
            {
                if(e %2 == 0)
                {
                    sortedNumbers[INDEX_EVEN].Add(e);
                } else
                {
                    sortedNumbers[INDEX_ODD].Add(e);
                }
            }
            Tuple<int[], int[]> sortedTuple = new Tuple<int[], int[]>(sortedNumbers[INDEX_ODD].ToArray(), sortedNumbers[INDEX_EVEN].ToArray());
            return sortedTuple;
        }

        /// <summary>
        /// Print event and odd numbers from a random generated number array
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            int[] numbers = GenerateRandomArray(COUNT_RANDOM_NUMBERS);
            Tuple<int[], int[]> sortedNumbers = PairImpair(numbers);

            StringBuilder[] arraySb = new StringBuilder[2];
            arraySb[INDEX_ODD] = new StringBuilder("Impair : ");
            arraySb[INDEX_EVEN] = new StringBuilder("Pair   : ");

            foreach(int e in sortedNumbers.Item1)
            {
                arraySb[INDEX_ODD].Append($"{e} ");
            }
            foreach (int e in sortedNumbers.Item2)
            {
                arraySb[INDEX_EVEN].Append($"{e} ");
            }

            // Print the 2 parts separately
            foreach (StringBuilder sb in arraySb)
            {
                Console.WriteLine(sb);
            }
        }
    }
}
