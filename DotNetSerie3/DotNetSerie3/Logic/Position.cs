﻿using DotNetSerie3.Manager;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie3.Logic
{
    /// <summary>
    /// Geographic Position (long, lat)
    /// </summary>
    public class GeographicPosition
    {

        /// <summary>
        /// Longitude of Position
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Latitude of Position
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Instanciate a new Position
        /// </summary>
        /// <param name="longitude">longitude of position</param>
        /// <param name="latitude">latitude of position</param>
        public GeographicPosition(double longitude, double latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        /// <summary>
        /// Print position as a string
        /// </summary>
        /// <returns>string representing position</returns>
        public override string ToString()
        {
            return $"({Longitude}, {Latitude})";
        }

        /// <summary>
        /// Calculate distance between 2 positions
        /// </summary>
        /// <param name="destination">destination to calcul distance</param>
        /// <returns>distance [km]</returns>
        public double DistanceTo(GeographicPosition destination)
        {
            var EARTH_RADIUS = 6371;
            var deltaLatitude = MathHelper.GetRadianFromDegrees(destination.Latitude - Latitude);
            var deltaLongitude = MathHelper.GetRadianFromDegrees(destination.Longitude - Longitude);
            var a = Math.Sin(deltaLatitude / 2) * Math.Sin(deltaLatitude / 2) +
                    Math.Cos(MathHelper.GetRadianFromDegrees(Latitude)) * Math.Cos(MathHelper.GetRadianFromDegrees(destination.Latitude)) *
                    Math.Sin(deltaLongitude / 2) * Math.Sin(deltaLongitude / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return EARTH_RADIUS * c; // Distance [km]
        }

    }
}
