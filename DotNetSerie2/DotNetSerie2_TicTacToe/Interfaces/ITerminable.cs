﻿using DotNetSerie2_TicTacToe.TicTacToe;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.Interfaces
{
    interface ITerminable
    {
        /// <summary>
        /// Define if Board has a Stroke of 3 same symbol (Cross or Circle)
        /// </summary>
        /// <returns>true if there is a Winner, false otherwise</returns>
        bool HasStrokeOf3();

        /// <summary>
        /// Define if User can play next turn
        /// </summary>
        /// <returns>Struct that show if game is finished and if there is a Winner</returns>
        EndGameStep CanPlay();
    }
}
