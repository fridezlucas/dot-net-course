﻿using DotNetSerie3.Logic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DotNetSerie3.Builder
{
#nullable enable

    /// <summary>
    /// Station Builder  
    /// Allow to build a station
    /// </summary>
    public class StationBuilder
    {
        /// <summary>
        /// Station to build
        /// </summary>
        private Station station;

        /// <summary>
        /// Init a new Station Builder
        /// </summary>
        public StationBuilder()
        {
            this.station = new Station();
        }

        /// <summary>
        /// Set station name
        /// </summary>
        /// <param name="name">name to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetName(string name)
        {
            station.Name = name;
            return this;
        }

        /// <summary>
        /// Set a domain
        /// </summary>
        /// <param name="domain">domain to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetDomain(string? domain)
        {
            station.Domain = domain;
            return this;
        }

        /// <summary>
        /// Set a canton
        /// </summary>
        /// <param name="canton">canton to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetCanton(string? canton)
        {
            if (canton == "")
            {
                station.Canton = null;
            }
            else
            {
                station.Canton = canton;
            }
            return this;
        }

        /// <summary>
        /// Set an altitude
        /// </summary>
        /// <param name="altitude">altitude to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetAltitude(string altitude)
        {
            if (altitude == "")
            {
                station.Altitude = null;
            }
            else
            {
                station.Altitude = Int32.Parse(altitude);
            }
            return this;
        }

        /// <summary>
        /// Set a max altitude
        /// </summary>
        /// <param name="altitudeMax">max altitude to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetAltitudeMax(string altitudeMax)
        {
            if (altitudeMax == "")
            {
                station.AltitudeMax = null;
            }
            else
            {
                station.AltitudeMax = Int32.Parse(altitudeMax);
            }
            return this;
        }

        /// <summary>
        /// Set a position
        /// </summary>
        /// <param name="latitude">latitude of position</param>
        /// <param name="longitude">longitude of position</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetPosition(string latitude, string longitude)
        {
            if (latitude == "" && longitude == "")
            {
                station.Position = null;
            }
            else
            {
                try
                {
                    station.Position = new GeographicPosition(double.Parse(latitude.Replace(".", ",")), double.Parse(longitude.Replace(".", ",")));
                }
                catch (Exception)
                {
                    station.Position = null;
                }
            }
            return this;
        }

        /// <summary>
        /// Set adult price
        /// </summary>
        /// <param name="tarif">price to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetTarifAdult(string tarif)
        {
            station.TarifAdult = Int32.Parse(tarif);
            return this;
        }

        /// <summary>
        /// Set child price
        /// </summary>
        /// <param name="tarif">price to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetTarifChild(string tarif)
        {
            station.TarifChild = Int32.Parse(tarif);
            return this;
        }

        /// <summary>
        /// Set number of lift
        /// </summary>
        /// <param name="liftNumber">number of lift to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetLiftNumber(string liftNumber)
        {
            if (liftNumber == "")
            {
                station.LiftNumber = null;
            }
            else
            {
                station.LiftNumber = Int32.Parse(liftNumber);
            }
            return this;
        }

        /// <summary>
        /// Set slope number
        /// </summary>
        /// <param name="slopeNumber">slope number to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetSlopeNumber(string slopeNumber)
        {
            if (slopeNumber == "")
            {
                station.SlopeNumber = null;
            }
            else
            {
                station.SlopeNumber = Int32.Parse(slopeNumber);
            }
            return this;
        }

        /// <summary>
        /// Set slope distance
        /// </summary>
        /// <param name="slopeDistance">slope distance to set</param>
        /// <returns>StationBuilder</returns>
        public StationBuilder SetSlopeDistance(string slopeDistance)
        {
            if (slopeDistance == "")
            {
                station.SlopeDistance = null;
            }
            else
            {
                station.SlopeDistance = Int32.Parse(slopeDistance);
            }
            return this;
        }

        /// <summary>
        /// Build the station
        /// </summary>
        /// <returns>built station</returns>
        public Station BuildStation()
        {
            return station;
        }
    }
}
