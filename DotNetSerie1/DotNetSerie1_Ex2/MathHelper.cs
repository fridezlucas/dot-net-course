﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DotNetSerie1_Ex2
{
    public static class MathHelper
    {
        /// <summary>
        /// Calculate Sqrt next approximation
        /// </summary>
        /// <param name="n">Number to calculate its sqrt</param>
        /// <param name="lastApproximation">Last calculated approximation</param>
        /// <returns>new approximation</returns>
        public static double CalculateNextApproximation(double n, double lastApproximation)
        {
            return (lastApproximation + n / lastApproximation) / 2;
        }

        /// <summary>
        /// Approximate a square root of a given number
        /// </summary>
        /// <param name="n">Number to approximate its square root</param>
        public static void ApproximateSqrt(double n)
        {
            int iterations = 1;

            double lastApproximation = n;
            double MAXIMAL_ERROR = n * 10e-9;
            double error = lastApproximation;
            double sqrt = Math.Sqrt(n);

            Stopwatch measure = Stopwatch.StartNew();
            while (error > MAXIMAL_ERROR)
            {
                ConsoleHelper.PrintMathIteration(iterations++, n, lastApproximation);
                lastApproximation = CalculateNextApproximation(n, lastApproximation);
                error = Math.Abs(lastApproximation - sqrt);
            }
            measure.Stop();
            double totalSeconds = (double)(measure.ElapsedMilliseconds) / 1000;

            ConsoleHelper.PrintSqrtApproximationResult(iterations, n, lastApproximation, error, totalSeconds);
        }
    }
}
