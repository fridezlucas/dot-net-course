﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DotNetSerie4_Ex2.Resources;

namespace DotNetSerie4_Ex2
{
    class Program
    {
        public static void PrintWithDeclarativeSyntax(List<Client> listClients)
        {
            Console.WriteLine("Declarative syntax :");
            var filteredClients = from c in listClients
                                  where c.Region == "Europe"
                                  orderby c.Ventes descending
                                  select c;

            foreach (var c in filteredClients)
            {
                Console.WriteLine(c);
            }
        }

        public static void PrintWithExtensionMethods(List<Client> listClients)
        {
            Console.WriteLine("\nExtension methods :");
            var filteredClients = listClients
                                    .Where(c => c.Region == "Europe")
                                    .OrderByDescending(c => c.Ventes);

            foreach (var c in filteredClients)
            {
                Console.WriteLine(c);
            }
        }

        public static List<Client> GetUsersFilteredBySalresRevenue(List<Client> listClients, int min, int max)
        {
            return listClients
                        .Where(c => c.Ventes > min && c.Ventes < max)
                        .OrderByDescending(c => c.Ventes)
                        .ToList();
        }

        static void Main(string[] args)
        {
            List<Client> listClients = ResourcesHelper.GetClientList();
            PrintWithDeclarativeSyntax(listClients);
            PrintWithExtensionMethods(listClients);
            List<Client> listFilteredClient = GetUsersFilteredBySalresRevenue(listClients, 4000, 5000);

            Console.WriteLine("\nFiltered method :");
            foreach (var c in listFilteredClient)
            {
                Console.WriteLine(c);
            }
        }
    }
}
