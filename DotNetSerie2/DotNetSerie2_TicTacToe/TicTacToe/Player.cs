﻿using DotNetSerie2_TicTacToe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.TicTacToe
{
    /// <summary>
    /// TicTactoe Player
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Symbol Option of Player
        /// </summary>
        public Symbol Option { get; set; }

        /// <summary>
        /// Player Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Player Score
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Instanciate a new Player
        /// </summary>
        /// <param name="name">Player Name</param>
        /// <param name="option">Player Symbol</param>
        public Player(string name, Symbol option)
        {
            Name = name;
            Option = option;
            Score = 0;
        }

        /// <summary>
        /// Create a Player with name asking
        /// </summary>
        /// <param name="symbol">Defined Symbol</param>
        /// <returns>new Player</returns>
        public static Player AskPlayerCreation(Symbol symbol)
        {
            return new Player(Printer.AskName(), symbol);
        }

        /// <summary>
        /// Create a Player with name and symbol asking
        /// </summary>
        /// <returns>new Player</returns>
        public static Player AskPlayerCreation()
        {
            return new Player(Printer.AskName(), Printer.AskSymbol());
        }

        /// <summary>
        /// Play on a TicTacToeBoard
        /// </summary>
        /// <param name="board">board on which Player draw his symbol</param>
        public void Play(Board board)
        {
            int number = Printer.AskUserACase(this, board);

            (int, int) cell = Board.GetCellAccordingNumber(number);
            board[cell.Item1][cell.Item2] = Option;
        }
    }
}
