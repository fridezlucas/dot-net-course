﻿using DotNetSerie2_TicTacToe.Extensions;
using DotNetSerie2_TicTacToe.Interfaces;
using DotNetSerie2_TicTacToe.TicTacToe;
using System;
using System.ComponentModel;
using System.IO;

namespace DotNetSerie2_TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            // Game components
            Player player1 = Player.AskPlayerCreation();
            Player player2 = Player.AskPlayerCreation(player1.Option == Symbol.Cross ? Symbol.Circle : Symbol.Cross);

            // Run game !
            Game game = new Game(player1, player2);
            game.Run();
        }
    }
}
