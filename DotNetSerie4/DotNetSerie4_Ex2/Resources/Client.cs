﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie4_Ex2
{
    public class Client
    {
        public string ID { get; set; }
        public string Ville { get; set; }
        public string Pays { get; set; }
        public string Region { get; set; }
        public decimal Ventes { get; set; }

        public override string ToString()
        {
            return "ID:" + ID + " Ville:" + Ville + "Pays:" + Pays +
                   " Region:" + Region + " Ventes:" + Ventes;
        }
    }
}
