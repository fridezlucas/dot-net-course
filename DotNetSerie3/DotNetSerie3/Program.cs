﻿using DotNetSerie3.Logic;
using DotNetSerie3.Manager;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotNetSerie3
{
    /// <summary>
    /// Main program of Serie 3
    /// </summary>
    class Program
    {
        /// <summary>
        /// Show all cantons in csv file
        /// </summary>
        /// <param name="lstStations">list of destination from csv file</param>
        private static void ShowAllCanton(List<Station> lstStations)
        {
            List<string> lstCantons = lstStations
                                        .Select(s => s.Canton)
                                        .Where(c => c != null)
                                        .Distinct()
                                        .OrderBy(c => c)
                                        .ToList();
            Printer.PrintAllCantons(lstCantons);
        }

        /// <summary>
        /// Show all stations in csv file according a sort (2*adultPrice + 2*childPrice < 150)
        /// </summary>
        /// <param name="lstStations">list of station from csv file</param>
        private static void ShowAllStation(List<Station> lstStations)
        {
            List<Station> lstStationsFamilyCostUnder150 = lstStations
                                                            .OrderBy(s => s.Canton)
                                                            .Where(s => s.TarifAdult * 2 + s.TarifChild * 2 < 150)
                                                            .ToList();
            Printer.PrintAllStations(lstStationsFamilyCostUnder150);
        }

        /// <summary>
        /// Show all stations near HE-Arc (under 150km bird distance)
        /// </summary>
        /// <param name="lstStations">list station to filter</param>
        private static void ShowAllStationNearHES(List<Station> lstStations)
        {
            GeographicPosition HeArcPosition = new GeographicPosition(46.997727, 6.938725);
            List<Station> lstStationsNearHES = lstStations
                                                    .OrderBy(s => s.Canton)
                                                    .Where(s => s.Position != null)
                                                    .Where(s => s.Position.DistanceTo(HeArcPosition) < 150)
                                                    .ToList();
            Printer.PrintAllStations(lstStationsNearHES);
        }

        /// <summary>
        /// Main Program
        /// </summary>
        /// <param name="args">args</param>
        static void Main(string[] args)
        {
            List<Station> lstStations = DataExtracter.GetStations();

            ShowAllCanton(lstStations);
            ShowAllStation(lstStations);
            ShowAllStationNearHES(lstStations);
        }
    }
}
