﻿using DotNetSerie2_TicTacToe.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNetSerie2_TicTacToe.TicTacToe
{
    /// <summary>
    /// Represent a TicTacToe Symbol to play. Empty, Cross or Circle
    /// </summary>
    public enum Symbol
    {
        [Description(" ")]
        [PlayFirst(false)]
        Empty = 0,

        [Description("O")]
        [PlayFirst(true)]
        Circle = 1,

        [Description("X")]
        [PlayFirst(false)]
        Cross = 2
    }
}
