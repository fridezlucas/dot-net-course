﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie3.Manager
{
    /// <summary>
    /// Math Helper
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Degrees to Radian
        /// </summary>
        /// <param name="degrees">degrees to transform</param>
        /// <returns>radians</returns>
        public static double GetRadianFromDegrees(double degrees)
        {
            return Math.PI / 180 * degrees;
        }
    }
}
