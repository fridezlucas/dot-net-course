﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie1_Ex2
{
    public static class ConsoleHelper
    {
        /// <summary>
        /// Print info program on console
        /// </summary>
        public static void WriteInfoProgram()
        {
            Console.WriteLine("-------------------");
            Console.WriteLine("| .NET Serie 1 Ex 2");
            Console.WriteLine("-------------------");
        }

        /// <summary>
        /// Ask User to write a number in console
        /// </summary>
        /// <param name="number"></param>
        public static void AskUserRealNumber(out double numberEntered)
        {
            numberEntered = -1;
            bool isCorrectNumber = false;
            do
            {
                Console.Write("Please write a real positive number : ");
                try
                {
                    numberEntered = Convert.ToDouble(Console.ReadLine());
                    isCorrectNumber = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Please enter a correct number !");
                }
            } while (!isCorrectNumber);

        }

        /// <summary>
        /// Print a math sqrt iteration
        /// </summary>
        /// <param name="i">Iteration number</param>
        /// <param name="n">Number to calculate its sqrt</param>
        /// <param name="approximation">Current approximation</param>
        public static void PrintMathIteration(int i, double n, double approximation)
        {
            Console.WriteLine($"[{i.ToString().PadLeft(4, '0')}] Approximate sqrt of number {n} : {approximation}");
        }

        /// <summary>
        /// Print Sqrt approximation result
        /// </summary>
        /// <param name="i">Number of iterations</param>
        /// <param name="n">Number to calculate its sqrt</param>
        /// <param name="approximation">Calculated approximation</param>
        /// <param name="error">Error with Math.Sqrt() method</param>
        /// <param name="s">Number of seconds</param>
        public static void PrintSqrtApproximationResult(int i, double n, double approximation, double error, double s)
        {
            Console.WriteLine($"\nSquare root of {n} is approximated to {approximation}.");
            Console.WriteLine($"It took {s}s with {i} iteration{(i > 1 ? "s" : "")}.");
            Console.WriteLine($"Residual error with `Math.Sqrt({n})` is {error}");
        }
    }
}
