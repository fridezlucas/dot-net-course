﻿using System;

namespace DotNetSerie1_Ex5
{
    class Program
    {
        /// <summary>
        /// Test comparisons wiht methods on strings
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string s1 = "Hello World";
            string s2 = "Hello World";
            string s3 = s1;

            // Equals => True = Same
            Console.WriteLine($"s1.Equals(s2) = {s1.Equals(s2)}");
            Console.WriteLine($"s1.Equals(s3) = {s1.Equals(s3)}");
            Console.WriteLine($"s2.Equals(s3) = {s2.Equals(s3)}");

            // CompareTo => 0 = Same
            Console.WriteLine($"s1.CompareTo(s2) = {s1.CompareTo(s2)}");
            Console.WriteLine($"s1.CompareTo(s3) = {s1.CompareTo(s3)}");
            Console.WriteLine($"s2.CompareTo(s3) = {s2.CompareTo(s3)}");

            // Equals => True = Same
            Console.WriteLine($"ReferenceEquals(s1, s2) = {ReferenceEquals(s1, s2)}");
            Console.WriteLine($"ReferenceEquals(s1, s3) = {ReferenceEquals(s1, s3)}");
            Console.WriteLine($"ReferenceEquals(s2, s3) = {ReferenceEquals(s2, s3)}");

            s1 += "!";
            s2 += "!";
            s3 += "!";

            // Equals v2
            Console.WriteLine($"s1.Equals(s2) = {s1.Equals(s2)}");
            Console.WriteLine($"s1.Equals(s3) = {s1.Equals(s3)}");
            Console.WriteLine($"s2.Equals(s3) = {s2.Equals(s3)}");

            // CompareTo v2
            Console.WriteLine($"s1.CompareTo(s2) = {s1.CompareTo(s2)}");
            Console.WriteLine($"s1.CompareTo(s3) = {s1.CompareTo(s3)}");
            Console.WriteLine($"s2.CompareTo(s3) = {s2.CompareTo(s3)}");

            // Equals v2
            Console.WriteLine($"ReferenceEquals(s1, s2) = {ReferenceEquals(s1, s2)}");
            Console.WriteLine($"ReferenceEquals(s1, s3) = {ReferenceEquals(s1, s3)}");
            Console.WriteLine($"ReferenceEquals(s2, s3) = {ReferenceEquals(s2, s3)}");
        }
    }
}
