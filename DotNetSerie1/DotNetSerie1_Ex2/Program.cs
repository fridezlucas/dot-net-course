﻿using System;

namespace DotNetSerie1_Ex2
{
    class Program
    {
        /// <summary>
        /// Approximate a square root of a number asked to the user
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            ConsoleHelper.WriteInfoProgram();
            ConsoleHelper.AskUserRealNumber(out double number);
            MathHelper.ApproximateSqrt(number);
        }
    }
}
