﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetSerie2_TicTacToe.Extensions
{
    /// <summary>
    /// Custom Attribute to define if a Symbol is the first or second to play
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class PlayFirstAttribute : System.Attribute
    {
        /// <summary>
        /// Attribute to define if symbol must play first
        /// </summary>
        public bool PlayFirst { get; set; }

        /// <summary>
        /// Create a new attribute
        /// </summary>
        /// <param name="playFirst">playFirst property</param>
        public PlayFirstAttribute(bool playFirst)
        {
            this.PlayFirst = playFirst;
        }
    }
}
